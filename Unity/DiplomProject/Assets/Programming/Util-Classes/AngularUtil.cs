﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class AngularUtil
{
    /// <summary>
    /// Converts angle and distance to a position in the 3d-space
    /// </summary>
    /// <param name="radius"> distance between center and returnpoint.</param>
    /// <param name="angle"> rotation relative to referancepoint.</param>
    /// <param name="refpoint"> point of referance.</param>
    /// <returns> Vector3 carthesian coordinates</returns>
    public static Vector3 PolarToCohesion(float radius, float angle, Vector3 refpoint = new Vector3())
    {
        float x = Mathf.Cos(angle * Mathf.Deg2Rad) * radius;
        float z = Mathf.Sin(angle * Mathf.Deg2Rad) * radius;
        return new Vector3(x + refpoint.x, refpoint.y, z + refpoint.z);
    }

    /// <summary>
    /// Method to map an angle to positive between 0 and 360
    /// </summary>
    /// <param name="angle"> angle to map.</param>
    /// <returns>Mapped angle</returns>
    public static float MapAgleToPositive(float angle, float min = 0, float max = 360)
    {
        if (angle < min)
            angle = max + angle;

        return angle %= max;
    }

}
