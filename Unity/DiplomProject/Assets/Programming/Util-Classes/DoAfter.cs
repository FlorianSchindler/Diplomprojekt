﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DoAfter {

	public static IEnumerator DoIt(System.Action operation, float delay){
		yield return new WaitForSeconds(delay);
		operation();
	}
}
