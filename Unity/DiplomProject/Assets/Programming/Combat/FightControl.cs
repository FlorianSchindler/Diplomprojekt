using UnityEngine;


[RequireComponent(typeof(Swipe))]
public class FightControl : MonoBehaviour
{
    private Swipe swipe;

    public CombatCalculation calc = null;
    public Visualizer visuals = null;

    public Transform player;
    public Transform guidepoint;
    public Transform enemy;

    [Header("Player Controls")]
    public float jumpDistance = 10;
    public float jumpspeed = 1;

    [Header("Angular Controls")]
    public float radius = 5;

    #region Properties
    private float _modelRot;
    public float ModelRot
    {
        get
        {
            return _modelRot;
        }
        set
        {
            _modelRot += value;
            _modelRot = AngularUtil.MapAgleToPositive(_guidePointRot);
        }
    }

    private float _guidePointRot;
    public float GuidePointRotation
    {
        get
        {
            return _guidePointRot;
        }
        set
        {
            _guidePointRot = value;
            _guidePointRot = AngularUtil.MapAgleToPositive(_guidePointRot);

            visuals.SetGuidePoint();
        }
    }
    #endregion

    private void Awake()
    {
        visuals = new Visualizer(this, player, guidepoint);
        swipe = GetComponent<Swipe>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.D) || swipe.SwipeRight)
        {
            if (!visuals.isLerping)
            {
                GuidePointRotation += jumpDistance;
                StartCoroutine(visuals.UpdatePlayerModel());
            }

        }
        else if (Input.GetKeyDown(KeyCode.A) || swipe.SwipeLeft)
        {
            if (!visuals.isLerping)
            {
                GuidePointRotation -= jumpDistance;
                StartCoroutine(visuals.UpdatePlayerModel());
            }
        }
        else if (swipe.SwipeDown)
        {

        }
        else if (swipe.SwipeUp)
        {

        }
    }

}





