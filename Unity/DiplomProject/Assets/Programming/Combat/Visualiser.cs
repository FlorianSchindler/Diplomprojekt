﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Visualizer
{
    //refs
    public FightControl fightControl;
    public Transform playerModel;
    public Transform guidePoint;

    public bool isLerping = false;

    #region  Constructors 

    public Visualizer(FightControl fControl)
    {
        this.fightControl = fControl;
    }

    public Visualizer(FightControl fightControl, Transform playerModel, Transform guidePoint)
    {
        this.fightControl = fightControl;
        this.playerModel = playerModel;
        this.guidePoint = guidePoint;
    }

    #endregion

    public IEnumerator UpdatePlayerModel()
    {

        float lerpTime = CalculateLerpTime();
        float curLerpTime = 0;
        float t = 0;

        float originalAngle = fightControl.ModelRot;

        isLerping = true;

        while (t < 1)
        {
            yield return null;


            curLerpTime += Time.deltaTime;
            if (curLerpTime > lerpTime)
            {
                curLerpTime = lerpTime;
                isLerping = false;
            }
            //MonoBehaviour.print(curLerpTime);

            t = curLerpTime / lerpTime;

            float curAngle = Mathf.LerpAngle(originalAngle, fightControl.GuidePointRotation, t) * Mathf.Deg2Rad;

            fightControl.ModelRot = curAngle;

            Vector3 newPlayerPos = AngularUtil.PolarToCohesion(fightControl.radius, curAngle * Mathf.Rad2Deg, fightControl.enemy.position);

            playerModel.transform.position = newPlayerPos;
        }
    }

    // XXX Fix this calculation
    public float CalculateLerpTime()
    {
        float deltaAngle = Mathf.DeltaAngle(Mathf.Abs(fightControl.GuidePointRotation), Mathf.Abs(fightControl.ModelRot));

        float lerpTime = Mathf.Abs(deltaAngle * Time.deltaTime * ( 1 / fightControl.jumpspeed));

        return lerpTime;
    }

    public void SetGuidePoint()
    {
        guidePoint.transform.position = AngularUtil.PolarToCohesion(fightControl.radius, fightControl.GuidePointRotation);
    }


}
