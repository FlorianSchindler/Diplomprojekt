﻿using UnityEngine;

public class CombatCalculation
{
    public Vector3 CalculatePlayerPosition(float angle, float radius)
    {
        return AngularUtil.PolarToCohesion(radius, angle);
    }

    public Quaternion CalculatePlayerRotation(Transform player, Transform enemy)
    {
        Vector3 enemyDir = enemy.position - player.position;

        Quaternion rotation = Quaternion.LookRotation(enemyDir.normalized,Vector3.up);

		return rotation;
    }

}
