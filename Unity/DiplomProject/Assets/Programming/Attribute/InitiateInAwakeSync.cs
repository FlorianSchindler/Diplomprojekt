using System;
using System.Reflection;
using UnityEngine;

public class InitiateInAwakeSync : MonoBehaviour
{
    private void Awake()
    {
        MonoBehaviour[] sceneActive = FindObjectsOfType<MonoBehaviour>();

        foreach (MonoBehaviour mono in sceneActive)
        {
            FieldInfo[] objectFields = mono.GetType().GetFields(BindingFlags.Instance | BindingFlags.Public);

            for (int i = 0; i < objectFields.Length; i++)
            {
                Assignattributes(objectFields[i], mono);
            }
        }
    }

    private void Assignattributes(FieldInfo finfo, MonoBehaviour mono)
    {
        AssignComponentInAwake attribute = Attribute.GetCustomAttribute(finfo, typeof(AssignComponentInAwake)) as AssignComponentInAwake;

        if (finfo != null)
        {
            if (finfo.FieldType.IsSubclassOf(typeof(Component)))
            {
                //Checking if the desired Component is on gameObject
                Component component = GetComponentType(finfo.FieldType, mono);
                if (component == null)
                {
                    //Adding Component 
                    Component newComp = mono.gameObject.AddComponent(finfo.FieldType);

                    DebugMessage(attribute, "Added new " + finfo.FieldType.ToString() + " to " + mono);

                    //assigning flaged attribute to new component
                    finfo.SetValue(mono, newComp);

                    DebugMessage(attribute, "Assigned " + finfo.FieldType.ToString() + " to " + mono);
                }
                else
                {
                    //assigning Component to flagged Attribute
                    finfo.SetValue(mono, component);

                    DebugMessage(attribute, "Assigned " + finfo.FieldType.ToString() + " to " + mono);
                }
            }
            //if its not a Component call defoult constructor
            else
            {
                //Calling Constructor
                ConstructorInfo ctor = finfo.FieldType.GetConstructor(new Type[] { });
                if (ctor != null)
                {
                    object instance = ctor.Invoke(new object[] { });
                    finfo.SetValue(mono, instance);
                }
            }
        }
    }

    private Component GetComponentType(Type type, MonoBehaviour mono)
    {
        Component[] components = mono.gameObject.GetComponents(typeof(Component));

        for (int x = 0; x < components.Length; x++)
        {
            if (components[x].GetType() == type)
            {
                return components[x];
            }
        }
        return null;
    }

    private void DebugMessage(AssignComponentInAwake comp, string text)
    {
        if (comp != null)

            if (comp.debugMessages)
            {
                Debug.Log(text);
            }
    }


}