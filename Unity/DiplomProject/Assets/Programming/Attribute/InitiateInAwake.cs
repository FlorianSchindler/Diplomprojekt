using System;
using UnityEngine;

[AttributeUsage(AttributeTargets.Field)]
public class AssignComponentInAwake : Attribute
{
    public bool debugMessages;

    public AssignComponentInAwake()
    {
        debugMessages = false;
    }

    public AssignComponentInAwake(bool debugMessages)
    {
        this.debugMessages = debugMessages;
    }
}